import React, { useState } from 'react';
import { fetchWeatherByCity } from '../utils/weather';
import { useDispatch, useSelector } from 'react-redux';
import * as actionTypes from '../store/actionTypes';
import weatherIcon from '../assets/weather.png';

const Home = () => {

    const [city, setCity] = useState('');
    const dispatch = useDispatch();

    const { weather } = useSelector(state => ({...state}))

    const handleCitySubmit = () => {
        console.log(city);
        fetchWeatherByCity(city)
            .then(res => {
                console.log(res);
                dispatch({
                    type: actionTypes.FETCH_WEATHER,
                    payload: {
                        cityWeather : res.data
                    }
                });
            }).catch(err => {
                console.log(err);
            })
    }

    return (
        <div className="container pt-5" style= {{ border: '1px solid black' }}>
            {/* Search BAR <br /> */}
            <div className="input-group mb-3">
                <input
                    type="text"
                    className="form-control"
                    placeholder="Enter City Name"
                    value={city}
                    onChange={e => setCity(e.target.value)}
                />
            <button
                className="btn btn-outline-secondary"
                type="button"
                id="button-addon2"
                onClick={handleCitySubmit}
            >Search</button>
            {/* {weather && JSON.stringify(weather.cityWeather.city)} */}
            </div>
            { weather && <div className="container">
                <div class="card" style={{width: "18rem"}}>
                    <img src={weatherIcon} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{weather.cityWeather.city.name}</h5>
                        <p className="card-text">Weather Report</p>
                        {
                            weather.cityWeather.list.map(item => (
                                <>
                                    <p key="">
                                        {item.main.temp}
                                        {item.weather[0].main}
                                        <img
                                            src={`http://openweathermap.org/img/w/${item.weather[0].icon}.png`}
                                            alt="new"
                                            style={{maxHeight: "30px", maxWidth: "30px"}}
                                        />
                                    </p>
                                </>
                            ))
                        }
                    </div>
                </div>
            </div>}
        </div>
    );
}

export default Home;