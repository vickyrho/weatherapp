import './App.css';
import Home from './components/Home';

const App = () => {
  return (
    <div className="App">
      <h1>WEATHER</h1>
        <Home />
      {/* MAIN CONTENT HERE  */}
    </div>
  );
}

export default App;
