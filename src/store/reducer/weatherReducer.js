import * as actionTypes from '../actionTypes';

export const weatherReducer = (state = null, action) => {
    switch(action.type) {
        case actionTypes.FETCH_WEATHER:
            return action.payload;
        default:
            return state;
    }   
}